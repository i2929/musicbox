package it.itsict.musicbox.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "album_tbl")
public class AlbumTbl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "TITLE")
    private String title;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ARTIST_ID")
    private ArtistTbl artist;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "album")
    private Set<TrackTbl> tracks;

    public int getId () {
        return id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {
        this.title = title;
    }

    public ArtistTbl getArtist () {
        return artist;
    }

    public void setArtist (ArtistTbl artist) {
        this.artist = artist;
    }

    public Set<TrackTbl> getTracks () {
        return tracks;
    }

    public void setTracks (Set<TrackTbl> tracks) {
        this.tracks = tracks;
    }
}
