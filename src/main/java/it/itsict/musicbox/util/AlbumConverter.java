package it.itsict.musicbox.util;

import it.itsict.musicbox.entities.AlbumTbl;
import it.itsict.musicbox.rest.json.Album;

import java.util.ArrayList;
import java.util.List;

public class AlbumConverter {

    public static Album convert (AlbumTbl albumTbl) {
        if (albumTbl == null) {
            return null;
        }

        Album result = new Album();
        result.setId(albumTbl.getId());
        result.setTitle(albumTbl.getTitle());
        if (albumTbl.getArtist() != null) {
            result.setArtistId(albumTbl.getArtist().getId());
        }
        return result;
    }

    public static List<Album> convertList (List<AlbumTbl> albumTblList) {
        if (albumTblList == null) {
            return new ArrayList<>();
        }

        List<Album> result = new ArrayList<>();
        for (AlbumTbl al : albumTblList) {
            Album album = convert(al);
            if (album != null) {
                result.add(album);
            }
        }
        return result;
    }
}
