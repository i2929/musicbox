package it.itsict.musicbox.util;

import it.itsict.musicbox.entities.ArtistTbl;
import it.itsict.musicbox.rest.json.Artist;

import java.util.ArrayList;
import java.util.List;

public class ArtistConverter {

    public static ArtistTbl convertToEntity (Artist artist) {
        if (artist == null) {
            return null;
        }

        ArtistTbl result = new ArtistTbl();
        result.setName(artist.getName());
        return result;
    }

    public static Artist convert (ArtistTbl artistTbl) {
        if (artistTbl == null) {
            return null;
        }

        Artist result = new Artist();
        result.setId(artistTbl.getId());
        result.setName(artistTbl.getName());
        return result;
    }

    public static List<Artist> convertList (List<ArtistTbl> artistTblList) {
        if (artistTblList == null) {
            return new ArrayList<>();
        }

        List<Artist> result = new ArrayList<>();
        for (ArtistTbl ar : artistTblList) {
            Artist artist = convert(ar);
            if (artist != null) {
                result.add(artist);
            }
        }
        return result;
    }
}
