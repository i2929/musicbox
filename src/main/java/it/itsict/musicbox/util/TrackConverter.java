package it.itsict.musicbox.util;

import it.itsict.musicbox.entities.TrackTbl;
import it.itsict.musicbox.rest.json.Track;

import java.util.ArrayList;
import java.util.List;

public class TrackConverter {

    public static Track convert(TrackTbl trackTbl) {
        if (trackTbl == null) {
            return null;
        }

        Track result = new Track();
        result.setId(trackTbl.getId());
        result.setTitle(trackTbl.getTitle());
        if(trackTbl.getAlbum() != null) {
            result.setAlbumId(trackTbl.getAlbum().getId());
        }
        return result;
    }

    public static List<Track> convertList(List<TrackTbl> trackTblList) {
        if (trackTblList == null) {
            return new ArrayList<>();
        }

        List<Track> result = new ArrayList<>();
        for (TrackTbl t : trackTblList) {
            Track track = convert(t);
            if (track != null) {
                result.add(track);
            }
        }
        return result;
    }
}
