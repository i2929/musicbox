package it.itsict.musicbox.rest;

import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/test")
public class MyService {

	private static final Logger LOG = Logger.getLogger(MyService.class);

	@Path("/ping")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String ping() {
		LOG.info("MyService ping");
		return "pong";
	}
}