package it.itsict.musicbox.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import it.itsict.musicbox.dao.ArtistDAO;
import it.itsict.musicbox.entities.ArtistTbl;
import it.itsict.musicbox.rest.json.Artist;
import it.itsict.musicbox.util.ArtistConverter;

@Path("/artist")
public class ArtistService {

	private static final Logger LOG = Logger.getLogger(ArtistService.class);

	@Path("/all")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Artist> getAll() {
		LOG.info("ArtistService getAll");

		ArtistDAO artistDAO = new ArtistDAO();
		List<ArtistTbl> artists = artistDAO.retrieveAll();
		return ArtistConverter.convertList(artists);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(Artist artist) {
		LOG.info("ArtistService create");

		if (artist == null) {
			LOG.error("ArtistService create: artist null");
			return Response.serverError().build();
		}

		ArtistTbl toCreate = ArtistConverter.convertToEntity(artist);
		ArtistDAO artistDAO = new ArtistDAO();
		ArtistTbl result = artistDAO.create(toCreate);

		if (result == null) {
			LOG.error("ArtistService create: result null");
			return Response.serverError().build();
		}

		return Response.ok(ArtistConverter.convert(result)).status(201).build();
	}
}
