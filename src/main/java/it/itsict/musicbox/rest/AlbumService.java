package it.itsict.musicbox.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import it.itsict.musicbox.dao.AlbumDAO;
import it.itsict.musicbox.entities.AlbumTbl;
import it.itsict.musicbox.rest.json.Album;
import it.itsict.musicbox.util.AlbumConverter;

@Path("/album")
public class AlbumService {

	private static final Logger LOG = Logger.getLogger(AlbumService.class);

	@Path("/all")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Album> getAll() {
		LOG.info("AlbumService getAll");

		AlbumDAO albumDAO = new AlbumDAO();
		List<AlbumTbl> albums = albumDAO.retrieveAll();
		return AlbumConverter.convertList(albums);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Album> getByAlbumId(@QueryParam("artistId") Integer artistId) {
		LOG.info("AlbumService getByAlbumId");
		AlbumDAO albumDAO = new AlbumDAO();
		List<AlbumTbl> albums = albumDAO.retrieveByArtistId(artistId);
		return AlbumConverter.convertList(albums);
	}
}
