package it.itsict.musicbox.rest.json;

public class Album {

    private Integer id;
    private String title;
    private Integer artistId;

    public Integer getId () {
        return id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {
        this.title = title;
    }

    public Integer getArtistId () {
        return artistId;
    }

    public void setArtistId (Integer artistId) {
        this.artistId = artistId;
    }
}
