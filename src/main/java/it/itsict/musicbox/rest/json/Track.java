package it.itsict.musicbox.rest.json;

import org.codehaus.jackson.annotate.JsonProperty;

public class Track {

    private Integer id;
    private String title;
    private Integer albumId;

    public Integer getId () {
        return id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {
        this.title = title;
    }

    public Integer getAlbumId () {
        return albumId;
    }

    public void setAlbumId (Integer albumId) {
        this.albumId = albumId;
    }
}
