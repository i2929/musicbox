package it.itsict.musicbox.rest;

import it.itsict.musicbox.dao.TrackDAO;
import it.itsict.musicbox.entities.TrackTbl;
import it.itsict.musicbox.rest.json.Track;
import it.itsict.musicbox.util.TrackConverter;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/track")
public class TrackService {

	private static final Logger LOG = Logger.getLogger(TrackService.class);

	@Path("/all")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Track> getAll() {
		LOG.error("TrackService getAll");

		TrackDAO trackDAO = new TrackDAO();
		List<TrackTbl> tracks = trackDAO.retrieveAll();
		return TrackConverter.convertList(tracks);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Track> getByAlbumId(@QueryParam("albumId") Integer albumId) {
		LOG.error("TrackService getByAlbumId");

		TrackDAO trackDAO = new TrackDAO();
		List<TrackTbl> tracks = trackDAO.retrieveByAlbumId(albumId);
		return TrackConverter.convertList(tracks);
	}
}