package it.itsict.musicbox.dao;

import it.itsict.musicbox.entities.AlbumTbl;
import it.itsict.musicbox.entities.ArtistTbl;
import it.itsict.musicbox.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class ArtistDAO {

    private static final Logger LOG = Logger.getLogger(ArtistDAO.class);

    private static final String RETRIEVE_ALL = "select a from ArtistTbl a";

    public List<ArtistTbl> retrieveAll () {
        List<ArtistTbl> result = new ArrayList<>();

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery(RETRIEVE_ALL);
            result = (List<ArtistTbl>) query.getResultList();
        } catch (Exception ex) {
            LOG.error("Error retrieving all artists", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

    public ArtistTbl create(ArtistTbl toCreate) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.save(toCreate);
            return toCreate;
        } catch (Exception ex) {
            LOG.error("Error saving artist", ex);
            return null;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

}
