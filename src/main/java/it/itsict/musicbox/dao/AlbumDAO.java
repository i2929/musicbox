package it.itsict.musicbox.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import it.itsict.musicbox.entities.AlbumTbl;
import it.itsict.musicbox.util.HibernateUtil;

public class AlbumDAO {

    private static final Logger LOG = Logger.getLogger(AlbumDAO.class);

    private static final String RETRIEVE_ALL = "select a from AlbumTbl a";
    private static final String RETRIEVE_BY_ALBUM_ID = "select a from AlbumTbl a where a.artist.id = :ARTISTID";

    public List<AlbumTbl> retrieveAll () {
        List<AlbumTbl> result = new ArrayList<>();

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery(RETRIEVE_ALL);
            result = (List<AlbumTbl>) query.getResultList();
        } catch (Exception ex) {
            LOG.error("Error retrieving all albums", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

    public List<AlbumTbl> retrieveByArtistId (Integer artistId) {
        if (artistId == null) {
            return retrieveAll();
        }

        List<AlbumTbl> result = new ArrayList<>();

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery(RETRIEVE_BY_ALBUM_ID);
            query.setParameter("ARTISTID", artistId); // set filter parameter
            result = (List<AlbumTbl>) query.getResultList();
        } catch (Exception ex) {
            LOG.error("Error retrieving albums by artist id", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

}
