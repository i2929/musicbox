package it.itsict.musicbox.dao;

import it.itsict.musicbox.entities.TrackTbl;
import it.itsict.musicbox.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class TrackDAO {

    private static final Logger LOG = Logger.getLogger(TrackDAO.class);

    private static final String RETRIEVE_ALL = "select t from TrackTbl t";
    private static final String RETRIEVE_BY_ALBUM_ID = "select t from TrackTbl t where t.album.id = :ALBUMID";

    public List<TrackTbl> retrieveAll() {
        List<TrackTbl> result = new ArrayList<>();

        Session session = null;
        try {
           session = HibernateUtil.getSessionFactory().openSession();
           Query query = session.createQuery(RETRIEVE_ALL);
           result = (List<TrackTbl>) query.getResultList();
        } catch (Exception ex) {
            LOG.error("Error retrieving all tracks", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

    public List<TrackTbl> retrieveByAlbumId(Integer albumId) {
        if (albumId == null) {
            return retrieveAll();
        }

        List<TrackTbl> result = new ArrayList<>();

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery(RETRIEVE_BY_ALBUM_ID);
            query.setParameter("ALBUMID", albumId); // set filter parameter
            result = (List<TrackTbl>) query.getResultList();
        } catch (Exception ex) {
            LOG.error("Error retrieving tracks by album id", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

}
